# Cloud

A fun little toy that lets us explore a vast Mandelbox fractal. Meshes are constructed using the marching cubes 
algorithm. An octree is used to provide variable LoD with required meshes being produced concurrently across 
multiple threads.

Because of the above implementation, LoD changes are jarring and seams are often visible.

![Mandel Box 1](images/mandel-1.png)

![Mandel Box 2](images/mandel-2.png)

![Mandel Box 3](images/mandel-3.png)

[Short exploration video](https://www.youtube.com/watch?v=hNllWerucTQ)

Enjoy!