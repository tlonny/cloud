#version 330

in vec3 color;
in vec3 frag_position;
out vec4 fragColor;

vec4 calcFog(vec3 pos, vec3 color)
{
    float distance = length(pos);
    float fogFactor = distance/1000;
    fogFactor = 1 - clamp( fogFactor, 0.0, 0.7 );
    return vec4(color.x*fogFactor, color.y*fogFactor, color.z*fogFactor,1f);
}
void main()
{
    fragColor = calcFog(frag_position,color);
}