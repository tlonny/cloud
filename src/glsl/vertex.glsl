#version 330

layout (location =0) in vec3 position;
layout (location =1) in vec3 normal;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

out vec3 color;
out vec3 frag_position;

void main()
{
    float amt = dot(normal, vec3(1, 1, 1)) * 0.2f + 0.7f;
    color = vec3(amt, amt, amt);
    vec4 pos = viewMatrix * modelMatrix * vec4(position,1.0);
    gl_Position = projectionMatrix * pos;
    frag_position = pos.xyz;
}