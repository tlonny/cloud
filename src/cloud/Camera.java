package cloud;

import cloud.graphics.Display;
import cloud.graphics.Shader;
import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;


public class Camera {

    public static Camera CAMERA = new Camera();

    private static float ASPECT_RATIO = (float)Display.WINDOW_WIDTH / Display.WINDOW_HEIGHT;
    private static float FOV = (float)Math.toRadians(70f);
    private static float PITCH_LIMIT = (float)Math.toRadians(85f);
    private static float NEAR_PLANE = 0.1f;
    private static float FAR_PLANE = 100000f;
    private static float MOUSE_SENSITIVITY = 1f/900;
    private static float SPEED = 1.5f;

    private Matrix4f viewMatrix;
    private Matrix4f projectionMatrix;
    private Matrix3f orientationMatrix;

    public Vector3f position;
    public Vector3f rotation;

    public Camera() {
        this.position = new Vector3f();
        this.rotation = new Vector3f();
        this.viewMatrix = new Matrix4f();
        this.orientationMatrix = new Matrix3f();
        this.projectionMatrix = new Matrix4f();
    }

    public void update() {
        this.rotation.x -= Display.DISPLAY.getDeltaMouseX() * MOUSE_SENSITIVITY;
        this.rotation.y -= Display.DISPLAY.getDeltaMouseY() * MOUSE_SENSITIVITY;
        this.rotation.y = Math.max(Math.min(this.rotation.y, PITCH_LIMIT),-PITCH_LIMIT);

        this.orientationMatrix.identity();
        this.orientationMatrix.rotateY(this.rotation.x);
        this.orientationMatrix.rotateX(this.rotation.y);

        if(Display.DISPLAY.isKeyDown(GLFW.GLFW_KEY_Q))
            SPEED += 0.1f;
        if(Display.DISPLAY.isKeyDown(GLFW.GLFW_KEY_E))
            SPEED -= 0.1f;
        SPEED = Math.max(0f, SPEED);

        if( Display.DISPLAY.isKeyDown(GLFW.GLFW_KEY_W))
            this.position.add(new Vector3f(0,0,-SPEED).mul(this.orientationMatrix));
        if( Display.DISPLAY.isKeyDown(GLFW.GLFW_KEY_S))
            this.position.add(new Vector3f(0,0,SPEED).mul(this.orientationMatrix));
        if( Display.DISPLAY.isKeyDown(GLFW.GLFW_KEY_A))
            this.position.add(new Vector3f(-SPEED,0,0).mul(this.orientationMatrix));
        if( Display.DISPLAY.isKeyDown(GLFW.GLFW_KEY_D))
            this.position.add(new Vector3f(SPEED,0,0).mul(this.orientationMatrix));

        this.viewMatrix.identity();
        this.viewMatrix.rotateX(-this.rotation.y);
        this.viewMatrix.rotateY(-this.rotation.x);
        this.viewMatrix.translate(this.position.mul(-1, new Vector3f()));

        this.projectionMatrix.identity();
        this.projectionMatrix.perspective(FOV, ASPECT_RATIO, NEAR_PLANE, FAR_PLANE);
    }

    public void prepare() {
        Shader.SHADER.setProjectionMatrix(this.projectionMatrix);
        Shader.SHADER.setViewMatrix(this.viewMatrix);
    }

}
