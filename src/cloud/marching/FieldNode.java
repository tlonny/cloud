package cloud.marching;

import cloud.Mandelbox;
import cloud.util.ThreadPool;
import cloud.Camera;
import cloud.graphics.Mesh;
import cloud.graphics.MeshData;
import cloud.graphics.Shader;
import cloud.util.ConcurrentBox;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import java.util.concurrent.Future;

public class FieldNode {

    private static int SCALE = 10;
    private static int NUM_CHUNKS_MIN = 3;
    private static int NUM_CHUNKS_MAX = 2;

    public static FieldNode FIELD_NODE = new FieldNode(SCALE);
    public static void init() {
        FIELD_NODE.setup();
    }


    // Shared across all entities as draws are done sequentially.
    private static Matrix4f modelMatrix = new Matrix4f();

    private static Vector3i[] CHILDREN_POSITION_TABLE = new Vector3i[] {
        new Vector3i(0,0,0),
        new Vector3i(0,0,1),
        new Vector3i(0,1,0),
        new Vector3i(0,1,1),
        new Vector3i(1,0,0),
        new Vector3i(1,0,1),
        new Vector3i(1,1,0),
        new Vector3i(1,1,1)
    };

    public Vector3i offset;
    public int scale;
    private boolean divided;
    private ConcurrentBox<MeshData> meshDataBox;
    private Future meshFuture;
    private Mesh mesh;
    private FieldNode[] children;

    public int getLength() {
        return ChunkData.NUM_EDGES * (1 << this.scale);
    }

    public FieldNode(int scale) {
        this(scale, new Vector3i(0, 0, 0));
    }

    public FieldNode(int scale, Vector3i offset) {
        this.scale = scale;
        this.offset = offset;
        this.meshDataBox = new ConcurrentBox<>();
        this.children = new FieldNode[8];
    }

    public void setup() {
        this.meshFuture = ThreadPool.THREAD_POOL.submit(this::buildMeshData);
    }

    private void reclaimChildren() {
        for(var ix = 0; ix < this.children.length; ix += 1 ) {
            this.children[ix].tearDown();
            this.children[ix] = null;
        }
        this.divided = false;
    }

    private void spawnChildren() {
        for(var ix = 0; ix < this.children.length; ix += 1 ) {
            var newScale = this.scale - 1;
            var newOffset = new Vector3i(CHILDREN_POSITION_TABLE[ix]).mul(ChunkData.NUM_EDGES << newScale).add(this.offset);
            this.children[ix] = new FieldNode(newScale, newOffset);
            this.children[ix].setup();
        }
        this.divided = true;
    }

    public void tearDown() {
        if(this.divided)
            this.reclaimChildren();
        this.meshFuture.cancel(false);
        if(this.mesh != null)
            this.mesh.tearDown();
    }

    public void update() {
        var cameraPos = Camera.CAMERA.position;
        var centreOffset = (ChunkData.NUM_EDGES << this.scale) / 2f;
        var centre = new Vector3f(offset).add(centreOffset, centreOffset, centreOffset);
        var distance = cameraPos.distance(centre);

        var divisionDistance = ChunkData.NUM_EDGES * (NUM_CHUNKS_MIN << this.scale);
        if(!this.divided && this.scale > 0 && distance < divisionDistance)
            this.spawnChildren();

        var recombinationDistance = ChunkData.NUM_EDGES * (NUM_CHUNKS_MAX << this.scale);
        if(this.divided && distance > recombinationDistance)
            this.reclaimChildren();

        if(this.mesh == null) {
            var meshData = this.meshDataBox.take();
            if (meshData != null) {
                this.mesh = new Mesh(meshData);
                this.mesh.setup();
            }
        }

        if(this.divided)
            for(var child : children)
                child.update();
    }

    private void buildMeshData() {
        var chunkData = new ChunkData();
        for(var x = 0; x < ChunkData.NUM_VERTICES; x += 1)  {
            for(var y = 0; y < ChunkData.NUM_VERTICES; y += 1) {
                for (var z = 0; z < ChunkData.NUM_VERTICES; z += 1) {
                    var chunkDataPosition = new Vector3i(x,y,z);
                    var absolutePosition = new Vector3f(chunkDataPosition)
                            .mul(1 << this.scale)
                            .add(new Vector3f(this.offset))
                            .div(FieldNode.FIELD_NODE.getLength());
                    var value = Mandelbox.MANDEL_BOX.getValue(absolutePosition);
                    chunkData.setValue(chunkDataPosition, value);
                }
            }
        }
        this.meshDataBox.put(chunkData.buildMeshData());
    }

    private boolean childrenReady() {
        for(var child : children) {
            if(child.mesh == null)
                return false;
        }
        return true;
    }

    public void render() {
        if(this.mesh == null)
            return;
        if(this.divided && this.childrenReady()) {
            for (var child : children)
                child.render();
            return;
        }
        modelMatrix.identity();
        modelMatrix.translate(new Vector3f(this.offset));
        modelMatrix.scale(1 << this.scale);
        Shader.SHADER.setModelMatrix(modelMatrix);
        this.mesh.render();
    }
}
