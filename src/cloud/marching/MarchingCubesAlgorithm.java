package cloud.marching;

import cloud.graphics.MeshData;
import org.joml.Vector3f;

public class MarchingCubesAlgorithm {

    private static float ISO_LEVEL = 0.2f;
    private static float EPSILON = 0.0001f;

    private static int getCubeIndex(float[] cubeField) {
       var cubeIndex = 0;
       for(var i = 0; i < 8; i += 1) {
           if (cubeField[i] < ISO_LEVEL)
               cubeIndex |= (1 << i);
       }
       return cubeIndex;
    }

    private static Vector3f interpolate(Vector3f start, Vector3f end, float valueStart, float valueEnd) {
        if(Math.abs(ISO_LEVEL - valueStart) < EPSILON)
            return start;
        if(Math.abs(ISO_LEVEL - valueEnd) < EPSILON)
            return end;
        if(Math.abs(valueStart - valueEnd) < EPSILON)
            return start;
        float scalar = (ISO_LEVEL - valueStart) / (valueEnd - valueStart);
        return new Vector3f(
            start.x + scalar * (end.x - start.x),
            start.y + scalar * (end.y - start.y),
            start.z + scalar * (end.z - start.z)
        );
    }

    public static void pushToMesh(MeshData meshData, Vector3f cubeCoords, float[] cubeField) {
        var cubeIndex = getCubeIndex(cubeField);
        var edgeData = MarchingCubeLookup.EDGE_TABLE[cubeIndex];
        var triangles = MarchingCubeLookup.TRIANGLE_TABLE[cubeIndex];
        var vertexList = new Vector3f[12];
        for( var i = 0; i < 12; i += 1 ) {
            if ((edgeData & (1 << i)) == 0)
                continue;
            var startVertex = MarchingCubeLookup.VERTEX_TABLE[i][0];
            var endVertex = MarchingCubeLookup.VERTEX_TABLE[i][1];
            vertexList[i] = interpolate(
                    new Vector3f(MarchingCubeLookup.POSITION_TABLE[startVertex]),
                    new Vector3f(MarchingCubeLookup.POSITION_TABLE[endVertex]),
                    cubeField[startVertex],
                    cubeField[endVertex]
            );
        }

        for(var j = 0; triangles[j] != -1; j+=3) {
            var edgeVec1 = vertexList[triangles[j+1]].sub(vertexList[triangles[j]], new Vector3f());
            var edgeVec2 = vertexList[triangles[j+2]].sub(vertexList[triangles[j]], new Vector3f());
            var cross = edgeVec2.cross(edgeVec1).normalize();
            for(var k = 0; k < 3; k += 1 ) {
                var position = vertexList[triangles[j+k]].add(cubeCoords,new Vector3f());
                meshData.pushVertex(position, cross);
            }
        }
    }



}
