package cloud.marching;

import cloud.graphics.MeshData;
import org.joml.Vector3f;
import org.joml.Vector3i;

public class ChunkData {

    public static int NUM_EDGES = 16;
    public static int NUM_VERTICES = NUM_EDGES + 1;

    private int getIndexFromPosition(Vector3i position) {
        var index = 0;
        index += position.x;
        index += position.y * NUM_VERTICES;
        index += position.z * NUM_VERTICES * NUM_VERTICES;
        return index;
    }

    public float[] scalarData;

    public ChunkData() {
        this.scalarData = new float[NUM_VERTICES * NUM_VERTICES * NUM_VERTICES];
    }

    public void setValue(Vector3i position, float value) {
        var index = getIndexFromPosition(position);
        this.scalarData[index] = value;
    }

    public MeshData buildMeshData() {
        var meshData = new MeshData();
        float[] cubeData = new float[8];
        for( var x = 0; x < NUM_EDGES; x += 1) {
            for (var y = 0; y < NUM_EDGES; y += 1) {
                for (var z = 0; z < NUM_EDGES; z += 1) {
                    for( var i = 0; i < 8; i += 1) {
                        var positionOffset = MarchingCubeLookup.POSITION_TABLE[i];
                        var position = new Vector3i(x,y,z).add(positionOffset);
                        var index = getIndexFromPosition(position);
                        cubeData[i] = scalarData[index];
                    }
                    MarchingCubesAlgorithm.pushToMesh(meshData, new Vector3f(x,y,z), cubeData);
                }
            }
        }
        return meshData;
    }

}
