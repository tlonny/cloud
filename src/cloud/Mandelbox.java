package cloud;

import org.joml.Vector3f;
import org.joml.Vector3i;

public class Mandelbox {

    public static Mandelbox MANDEL_BOX = new Mandelbox();

    private int MAX_BOUNDS = 10000;
    private int ITERATIONS = 12;

    public float getValue(Vector3f position) {
        return this.isInFractal(new Vector3i(
            (int)(position.x * MAX_BOUNDS),
            (int)(position.y * MAX_BOUNDS),
            (int)(position.z * MAX_BOUNDS)
        )) ? 1f : -1f;
    }

    private boolean isInFractal(Vector3i position) {

        if (position.x >= MAX_BOUNDS || position.y >= MAX_BOUNDS || position.z >= MAX_BOUNDS) {
            return false;
        }
        if (position.x <= 0 || position.y <= 0 || position.z <= 0)
            return false;

        var scaledPosition = new Vector3f(position).div(MAX_BOUNDS);
        var x = (double) scaledPosition.x;
        var y = (double) scaledPosition.y;
        var z = (double) scaledPosition.z;
        float d = 0.001f;

        int s = 7;
        x *= s;
        y *= s;
        z *= s;
        d *= s;

        double posX = x;
        double posY = y;
        double posZ = z;

        double dr = 1.0;
        double r = 0.0;

        double scale = 2d;

        double minRadius2 = 0.25;
        double fixedRadius2 = 1;

        for (int n = 0; n < ITERATIONS; n++) {
            // Reflect
            if (x > 1.0)
                x = 2.0 - x;
            else if (x < -1.0)
                x = -2.0 - x;
            if (y > 1.0)
                y = 2.0 - y;
            else if (y < -1.0)
                y = -2.0 - y;
            if (z > 1.0)
                z = 2.0 - z;
            else if (z < -1.0)
                z = -2.0 - z;

            // Sphere Inversion
            double r2 = x * x + y * y + z * z;

            if (r2 < minRadius2) {
                x = x * fixedRadius2 / minRadius2;
                y = y * fixedRadius2 / minRadius2;
                z = z * fixedRadius2 / minRadius2;
                dr = dr * fixedRadius2 / minRadius2;
            } else if (r2 < fixedRadius2) {
                x = x * fixedRadius2 / r2;
                y = y * fixedRadius2 / r2;
                z = z * fixedRadius2 / r2;
                fixedRadius2 *= fixedRadius2 / r2;
            }

            x = x * scale + posX;
            y = y * scale + posY;
            z = z * scale + posZ;
            dr *= scale;
        }
        r = Math.sqrt(x * x + y * y + z * z);
        return (r / Math.abs(dr)) < d;
    }
}

