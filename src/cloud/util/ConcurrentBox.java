package cloud.util;

public class ConcurrentBox<T> {

    private T item;
    public synchronized void put(T item) {
        this.item = item;
    }

    public synchronized T take() {
        var item = this.item;
        this.item = null;
        return item;
    }

}
