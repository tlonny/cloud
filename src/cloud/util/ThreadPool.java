package cloud.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadPool {

    public static ThreadPool THREAD_POOL = new ThreadPool();

    private ExecutorService executorService = Executors.newFixedThreadPool(4);

    public void tearDown() {
       this.executorService.shutdown();
    }

    public Future submit(Runnable callable) {
        return this.executorService.submit(callable);
    }
}
