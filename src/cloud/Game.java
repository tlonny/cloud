package cloud;

import cloud.marching.FieldNode;
import cloud.graphics.Display;
import cloud.graphics.Shader;
import cloud.util.ThreadPool;
import org.joml.Vector3f;

public class Game {

    private static long UPDATE_MS = 10;

    private long simulatedTime;

    public void setup() {
        Display.init();
        Shader.init();
        FieldNode.init();

        var length = FieldNode.FIELD_NODE.getLength();
        Camera.CAMERA.position = new Vector3f(length/2, length/2, -100f);
    }

    private void update() {
        long currentTime = System.currentTimeMillis();
        while(simulatedTime < currentTime) {
            FieldNode.FIELD_NODE.update();
            Camera.CAMERA.update();
            simulatedTime += UPDATE_MS;
        }
    }

    private void draw() {
        Shader.SHADER.bind();
        Camera.CAMERA.prepare();
        FieldNode.FIELD_NODE.render();
        Display.DISPLAY.refresh();
    }

    public void run() {
        this.simulatedTime = System.currentTimeMillis();
        while(!Display.DISPLAY.shouldClose()) {
            this.update();
            this.draw();
        }
    }


    public static void main(String[] args) {
        var game = new Game();
        game.setup();
        game.run();
        ThreadPool.THREAD_POOL.tearDown();
    }

}
