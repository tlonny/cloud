package cloud.graphics;

import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class MeshData {

    public List<Vector3f> positions;
    public List<Vector3f> normals;
    public List<Integer> indices;
    private int nextIndex;

    public MeshData() {
        this.positions = new ArrayList<>();
        this.normals = new ArrayList<>();
        this.indices = new ArrayList<>();
    }

    public int pushVertex(Vector3f position, Vector3f normal) {
        var index = this.nextIndex;
        this.nextIndex += 1;
        this.positions.add(position);
        this.normals.add(normal);
        this.indices.add(index);
        return index;
    }

    public void repeatVertex(int index) {
        this.indices.add(index);
    }

}
