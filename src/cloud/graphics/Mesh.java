package cloud.graphics;

import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryUtil;

import java.util.ArrayList;
import java.util.List;

public class Mesh {

    private int vertexArrayID;
    private int numIndices;
    private List<Integer> vertexBufferIds;
    private MeshData meshData;

    public Mesh(MeshData meshData) {
        this.vertexBufferIds = new ArrayList<>();
        this.meshData = meshData;
    }

    public void setup() {
        this.vertexArrayID = GL30.glGenVertexArrays();
        this.bind();
        this.pushIndexData(this.meshData.indices);
        this.pushVertexData(0, this.meshData.positions);
        this.pushVertexData(1, this.meshData.normals);
        this.meshData = null;
    }

    private void pushIndexData(List<Integer> indices) {
        this.numIndices = indices.size();
        var buffer = MemoryUtil.memAllocInt(this.numIndices);
        for(var index : indices)
            buffer.put(index);
        buffer.flip();
        var vertexBufferID = GL30.glGenBuffers();
        GL30.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vertexBufferID);
        GL30.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        MemoryUtil.memFree(buffer);
    }

    private void pushVertexData(int index, List<Vector3f> vertices) {
        var buffer = MemoryUtil.memAllocFloat(vertices.size()*3);
        for(var vertex : vertices)
            buffer.put(vertex.x).put(vertex.y).put(vertex.z);
        buffer.flip();
        var vertexBufferID = GL30.glGenBuffers();
        GL30.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferID);
        GL30.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        GL30.glVertexAttribPointer(index, 3, GL11.GL_FLOAT, false, 0, 0);
        GL30.glEnableVertexAttribArray(index);
        this.vertexBufferIds.add(vertexBufferID);
        MemoryUtil.memFree(buffer);
    }

    private void bind() {
        GL30.glBindVertexArray(this.vertexArrayID);
    }

    public void tearDown() {
        GL20.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        for(var vertexBufferId : this.vertexBufferIds)
            GL20.glDeleteBuffers(vertexBufferId);
        GL30.glBindVertexArray(0);
        GL30.glDeleteVertexArrays(this.vertexArrayID);
    }

    public void render() {
        GL30.glBindVertexArray(this.vertexArrayID);
        GL30.glDrawElements(GL11.GL_TRIANGLES, this.numIndices, GL11.GL_UNSIGNED_INT, 0);
    }

}
