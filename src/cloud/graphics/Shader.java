package cloud.graphics;

import cloud.util.File;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL20;
import org.lwjgl.system.MemoryUtil;

public class Shader {

    public static Shader SHADER = new Shader();
    public static void init() {
        SHADER.setup();
    }

    private int programId;
    private int viewMatrixUniform;
    private int projectionMatrixUniform;
    private int modelMatrixUniform;

    public void setup() {
        this.programId = GL20.glCreateProgram();
        var vertexCode = this.getVertexShaderCode();
        this.createShader(vertexCode, GL20.GL_VERTEX_SHADER);
        var fragmentCode = this.getFragmentShaderCode();
        this.createShader(fragmentCode, GL20.GL_FRAGMENT_SHADER);
        GL20.glLinkProgram(this.programId);
        GL20.glValidateProgram(this.programId);
        this.projectionMatrixUniform = this.createUniform("projectionMatrix");
        this.viewMatrixUniform = this.createUniform("viewMatrix");
        this.modelMatrixUniform = this.createUniform("modelMatrix");
    }

    protected int createUniform(String uniformName) {
        return GL20.glGetUniformLocation(this.programId, uniformName);
    }

    protected void setUniform(int uniformLocation, Matrix4f value) {
        var buffer = MemoryUtil.memAllocFloat(16);
        value.get(buffer);
        GL20.glUniformMatrix4fv(uniformLocation, false, buffer);
        MemoryUtil.memFree(buffer);
    }

    private int createShader(String shaderCode, int shaderType) {
        var shaderId = GL20.glCreateShader(shaderType);
        GL20.glShaderSource(shaderId, shaderCode);
        GL20.glCompileShader(shaderId);
        GL20.glAttachShader(this.programId, shaderId);
        return shaderId;
    }

    public void bind() {
        GL20.glUseProgram(this.programId);
    }

    public void tearDown() {
        GL20.glUseProgram(0);
        GL20.glDeleteProgram(this.programId);
    }

    public void setProjectionMatrix(Matrix4f matrix) {
        this.setUniform(this.projectionMatrixUniform, matrix);
    }

    public void setViewMatrix(Matrix4f matrix) {
        this.setUniform(this.viewMatrixUniform, matrix);
    }

    public void setModelMatrix(Matrix4f matrix) {
        this.setUniform(this.modelMatrixUniform, matrix);
    }

    private String getVertexShaderCode() {
        return File.readStringFromFile("src/glsl/vertex.glsl");
    }

    private String getFragmentShaderCode() {
        return File.readStringFromFile("src/glsl/fragment.glsl");
    }


}
