package cloud.graphics;

import java.nio.IntBuffer;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

public class Display {

    public static Display DISPLAY = new Display();
    public static void init() {
        DISPLAY.setup();
    }

    private long windowId;

    private boolean[] keys;
    private double[] xBuffer;
    private double[] yBuffer;

    public static int WINDOW_WIDTH = 1280;
    public static int WINDOW_HEIGHT = 720;
    private static String WINDOW_TITLE = "Cloud";

    private Display() {
        this.keys = new boolean[500];
        this.xBuffer = new double[1];
        this.yBuffer = new double[1];
    }

    public float getMouseX() {
        return (float)this.xBuffer[0];
    }

    public float getMouseY() {
        return (float)this.yBuffer[0];
    }

    public float getDeltaMouseX() {
        return this.getMouseX() - WINDOW_WIDTH/2;
    }

    public float getDeltaMouseY() {
        return this.getMouseY() - WINDOW_HEIGHT/2;
    }

    private void onKeyEvent( long window, int key, int scancode, int action, int mods ) {
        this.keys[ key ] = action != GLFW.GLFW_RELEASE;
    }

    public boolean isKeyDown( int code ) {
        return this.keys[ code ];
    }

    public boolean shouldClose() {
        return GLFW.glfwWindowShouldClose( this.windowId );
    }

    public void refresh() {
        GLFW.glfwSwapBuffers( this.windowId );
        GLFW.glfwPollEvents();
        GL15.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GL15.glClear( GL15.GL_COLOR_BUFFER_BIT | GL15.GL_DEPTH_BUFFER_BIT );

        GLFW.glfwGetCursorPos( this.windowId, this.xBuffer, this.yBuffer );
        GLFW.glfwSetCursorPos(this.windowId, WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
        if( this.isKeyDown( GLFW.GLFW_KEY_ESCAPE ) )
            GLFW.glfwSetWindowShouldClose( this.windowId, true );
    }

    public void setup() {
        GLFWErrorCallback.createPrint(System.err).set();
        GLFW.glfwInit();
        GLFW.glfwWindowHint( GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE );
        GLFW.glfwWindowHint( GLFW.GLFW_RESIZABLE, GLFW.GLFW_FALSE );

        this.windowId = GLFW.glfwCreateWindow( WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, MemoryUtil.NULL, MemoryUtil.NULL );
        GLFW.glfwSetKeyCallback( this.windowId, this::onKeyEvent );

        try ( MemoryStack stack = MemoryStack.stackPush() ) {
            IntBuffer width  = stack.mallocInt(1);
            IntBuffer height = stack.mallocInt(1);
            GLFW.glfwGetWindowSize( this.windowId, width, height);
            GLFWVidMode vidMode = GLFW.glfwGetVideoMode( GLFW.glfwGetPrimaryMonitor() );
            GLFW.glfwSetWindowPos(
                this.windowId,
                (vidMode.width() - width.get(0) )/2,
                (vidMode.height() - height.get(0) )/2
            );
        }

        GLFW.glfwMakeContextCurrent( this.windowId );
        GLFW.glfwSwapInterval(1);
        GLFW.glfwShowWindow(this.windowId);
        GLFW.glfwSetInputMode(this.windowId, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_HIDDEN);
        GL.createCapabilities();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_FRONT);
    }
}